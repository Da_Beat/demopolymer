let express = require('express'),
  app = express(),
  port = process.env.PORT || 3000

let path = require('path')

let cors = require('cors')
app.use(cors())

app.use(express.static(__dirname + '/build/default'))

app.listen(port)

console.log(`Proyecto Polymer con wrapper de node.js en puerto ${port}`)

app.get('/', function(req, res) {
  res.sendFile('index.html', {root: '.'})
})
